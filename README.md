# Plexified subs

This subtitles are mostly updated mirrors of the subs that can be found at the official [AISubs repository](https://gitlab.com/ozonorei/subtitles)
However those subs are updated using the latest AI models improvements and some custom modifications. Sadly,
I don't keep the original files name, so i can't provide the original files name in this repo. I mainly watch the content
via media servers such as Plex, Jellyfin or Emby. And those subs are generated directly from my media storage backend.
Thus the naming is certainly different than original release names. However the do follow a certain patterns.

* Subs inside `variety_shows` directory follows the mostly the same naming as the release group `YY?YYmmdd show title (ep(\d))? (\- title)? [media tags]?`.
* Subs inside `YouTube` directory follows This standard `YYYYMMDD - title [youtube-(id)]` format.

Do keep in mind this repository only updated once in a while it's not as active as the main AISubs repository. 
